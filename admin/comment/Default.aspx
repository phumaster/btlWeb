﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="admin_comment_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Bình luận
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4>Bình luận</h4>
                <hr />
                <asp:GridView ID="comments" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource1" CssClass="table">
                    <Columns>
                        <asp:TemplateField HeaderText="ID" InsertVisible="False" SortExpression="id">
                            <EditItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nội dung" SortExpression="content">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("content") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("content") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Đăng lúc" SortExpression="created_at">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("created_at") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("created_at") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Người đăng" SortExpression="username">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("username") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("username") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ID bài viết" SortExpression="post_id">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("post_id") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Bind("post_id") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete" Text="Xóa" OnClientClick="return confirm('Bạn có thực sự muốn xóa!')"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:btlWebConnectionString %>" DeleteCommand="DELETE FROM [comments] WHERE [id] = @original_id AND (([content] = @original_content) OR ([content] IS NULL AND @original_content IS NULL)) AND (([created_at] = @original_created_at) OR ([created_at] IS NULL AND @original_created_at IS NULL)) AND (([username] = @original_username) OR ([username] IS NULL AND @original_username IS NULL)) AND (([post_id] = @original_post_id) OR ([post_id] IS NULL AND @original_post_id IS NULL))" InsertCommand="INSERT INTO [comments] ([content], [created_at], [username], [post_id]) VALUES (@content, @created_at, @username, @post_id)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [comments]" UpdateCommand="UPDATE [comments] SET [content] = @content, [created_at] = @created_at, [username] = @username, [post_id] = @post_id WHERE [id] = @original_id AND (([content] = @original_content) OR ([content] IS NULL AND @original_content IS NULL)) AND (([created_at] = @original_created_at) OR ([created_at] IS NULL AND @original_created_at IS NULL)) AND (([username] = @original_username) OR ([username] IS NULL AND @original_username IS NULL)) AND (([post_id] = @original_post_id) OR ([post_id] IS NULL AND @original_post_id IS NULL))">
                    <DeleteParameters>
                        <asp:Parameter Name="original_id" Type="Int32" />
                        <asp:Parameter Name="original_content" Type="String" />
                        <asp:Parameter Name="original_created_at" Type="DateTime" />
                        <asp:Parameter Name="original_username" Type="String" />
                        <asp:Parameter Name="original_post_id" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="content" Type="String" />
                        <asp:Parameter Name="created_at" Type="DateTime" />
                        <asp:Parameter Name="username" Type="String" />
                        <asp:Parameter Name="post_id" Type="Int32" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="content" Type="String" />
                        <asp:Parameter Name="created_at" Type="DateTime" />
                        <asp:Parameter Name="username" Type="String" />
                        <asp:Parameter Name="post_id" Type="Int32" />
                        <asp:Parameter Name="original_id" Type="Int32" />
                        <asp:Parameter Name="original_content" Type="String" />
                        <asp:Parameter Name="original_created_at" Type="DateTime" />
                        <asp:Parameter Name="original_username" Type="String" />
                        <asp:Parameter Name="original_post_id" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foot" runat="Server">
</asp:Content>

