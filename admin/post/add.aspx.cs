﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_post_add : System.Web.UI.Page
{
    DataControl data = new DataControl();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            DataTable tb = data.listCategories();
            drCategories.DataSource = tb;
            drCategories.DataTextField = "title";
            drCategories.DataValueField = "id";
            DataBind();
        }
    }

    protected void btnAddPost_handleClick(object sender, EventArgs e)
    {
        string thumbnail_url = "/asset/images/no-image.svg";
        if(thumbnail.HasFile)
        {
            String fileName = "/upload/thumbnails/" + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") + thumbnail.FileName;
            String filePath = MapPath(fileName);
            thumbnail.SaveAs(filePath);
            thumbnail_url = fileName;
        }
        Post post = new Post();
        User user = (User)Session["user"];
        post.title = title.Text;
        post.content = content.Text;
        post.created_at = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        post.see = 0;
        post.category_id = Convert.ToInt32(drCategories.SelectedValue);
        post.thumbnail_url = thumbnail_url;
        post.user_id = user.getID();
        data.addPost(post);
        lbMessage.Text = "Đã đăng bài viết.";
    }
}