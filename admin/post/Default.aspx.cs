﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_post_Default : System.Web.UI.Page
{
    DataControl data = new DataControl();
    protected void Page_Load(object sender, EventArgs e)
    {
        string page = Request.QueryString["page"];
        int currentPage = 1;
        if (page != null)
        {
            currentPage = Convert.ToInt32(page);
        }
        if (!IsPostBack)
        {
            DataTable posts = data.paginatePosts(currentPage);
            listPost.DataSource = posts;
            DataBind();

            string paginate = data.paginate("posts", currentPage);
            pagination.InnerHtml = paginate;
        }
    }

    protected void deleteItems(object sender, CommandEventArgs e)
    {
        string page = Request.QueryString["page"];
        int currentPage = 1;
        if (page != null)
        {
            currentPage = Convert.ToInt32(page);
        }
        if (e.CommandName == "destroy")
        {
            int id = Convert.ToInt32(e.CommandArgument);
            Post post = data.getPost(id);
            if(post.thumbnail_url != "/asset/images/no-image.svg")
            {
                FileInfo img = new FileInfo(MapPath(post.thumbnail_url));
                img.Delete();
            }
            data.destroyPost(id);
            Session["flash"] = "<div class=\"text-success\">Đã xóa bài viết.</div>";
            DataTable posts = data.paginatePosts(currentPage);
            listPost.DataSource = posts;
            DataBind();
        }
    }
}