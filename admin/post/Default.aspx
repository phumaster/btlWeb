﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="admin_post_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    Tin tức
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" Runat="Server">
     <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="control">
                    <a href="add.aspx" class="btn btn-primary"><i class="fa fa-pencil"></i>Viết bài mới</a>
                </div>
                <asp:GridView ID="listPost" runat="server" AutoGenerateColumns="False" CssClass="table">
                    <Columns>
                        <asp:BoundField HeaderText="ID" DataField="id" />
                        <asp:BoundField HeaderText="Tiêu đề" DataField="title" />
                        <asp:BoundField HeaderText="Ảnh nổi bật" DataField="thumbnail_url"/>
                        <asp:BoundField HeaderText="Danh mục" DataField="category_id" />
                        <asp:BoundField HeaderText="ID Người đăng" DataField="user_id" />
                        <asp:TemplateField HeaderText="Xóa">
                            <ItemTemplate>
                                <asp:Button ID="Button1" runat="server" Text="Xóa" OnCommand="deleteItems" CommandName="destroy" CommandArgument='<%# Eval("id") %>' CssClass="btn btn-link text-danger" OnClientClick="return confirm('Bạn có thực sự muốn xóa?')"/>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:HyperLinkField DataNavigateUrlFields="id" DataNavigateUrlFormatString="/admin/post/edit.aspx?id={0}" HeaderText="Sửa" Text="Sửa"/>
                    </Columns>
                </asp:GridView>
                <div class="text-center">
                    <literal runat="server" ID="pagination"></literal>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foot" Runat="Server">
</asp:Content>

