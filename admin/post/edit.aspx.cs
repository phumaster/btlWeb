﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_post_edit : System.Web.UI.Page
{
    DataControl data = new DataControl();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable tb = data.listCategories();
            drCategories.DataSource = tb;
            drCategories.DataTextField = "title";
            drCategories.DataValueField = "id";
            DataBind();
            try
            {
                int id = Convert.ToInt32(Request.QueryString["id"]);
                Post post = data.getPost(id);
                title.Text = post.title;
                content.Text = post.content;
                ltThumbnail.InnerHtml = "<img src=\""+post.thumbnail_url+"\" style=\"max-width: 80px\"/>";
                hiddenThumbnail.Value = post.thumbnail_url;
            } catch (Exception ex)
            {
                Response.Write("<script>alert('Bài viết không tồn tại hoặc đã bị xóa."+ex.Message+"')</script>");
            }
        }
    }

    protected void savePost_handleClick(object sender, EventArgs e)
    {
        try
        {
            int id = Convert.ToInt32(Request.QueryString["id"]);
            string thumbnail_url = hiddenThumbnail.Value;
            if (thumbnail.HasFile)
            {
                String fileName = "/upload/thumbnails/" + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") + thumbnail.FileName;
                String filePath = MapPath(fileName);
                thumbnail.SaveAs(filePath);
                thumbnail_url = fileName;
            }
            Post post = new Post();
            User user = (User)Session["user"];
            post.title = title.Text;
            post.content = content.Text;
            post.created_at = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            post.category_id = Convert.ToInt32(drCategories.SelectedValue);
            post.thumbnail_url = thumbnail_url;
            post.user_id = user.getID();
            data.updatePost(post, id);
            lbMessage.Text = "Đã cập nhật bài viết.";
            Session["flash"] = "<div class=\"text-success\">Đã cập nhật bài viết.</div>";
            Response.Redirect("/admin/post/Default.aspx");
        } catch (Exception ex)
        {
            Response.Redirect("/admin/post/Default.aspx");
        }
    }
}