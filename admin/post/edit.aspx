﻿<%@ Page ValidateRequest="false" Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="edit.aspx.cs" Inherits="admin_post_edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    Edit
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" Runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <asp:Label ID="lbMessage" runat="server" Text="" CssClass="text-success"></asp:Label>
                <div class="form-group">
                    <label>Tiêu đề</label>
                    <asp:TextBox runat="server" ID="title" CssClass="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="title" ErrorMessage="Vui lòng nhập tiêu đề." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group">
                    <label>Nội dung</label>
                    <asp:TextBox runat="server" ID="content" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label>Ảnh nổi bật</label>
                    <asp:FileUpload runat="server" ID="thumbnail"/>
                    <literal runat="server" id="ltThumbnail"></literal>
                    <asp:HiddenField runat="server" ID="hiddenThumbnail" Value="" />
                </div>
                <div class="form-group">
                    <label>Danh mục</label>
                    <br />
                    <asp:DropDownList ID="drCategories" runat="server"></asp:DropDownList>
                </div>
                <div class="form-group">
                    <asp:Button runat="server" ID="btnAddPost" Text="Lưu thay đổi" CssClass="btn btn-primary" OnClick="savePost_handleClick"/>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foot" Runat="Server">
    <script type="text/javascript">
        CKEDITOR.replace('body_content');
    </script>
</asp:Content>

