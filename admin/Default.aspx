﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="admin_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    Bảng điểu khiển
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" Runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4>Chào admin, <asp:Label ID="lbWelcome" runat="server" Text="Label"></asp:Label></h4>
                <hr />
                <div class="action">
                    <a href="/admin/post/add.aspx" class="btn btn-info"><i class="fa fa-pencil"></i> viết bài mới</a>
                    <a href="/admin/category/add.aspx" class="btn btn-warning"><i class="fa fa-plus"></i> thêm danh mục</a>
                    <a href="/admin/user/register.aspx" class="btn btn-primary"><i class="fa fa-user-plus"></i> thêm người dùng</a>
                    <a href="/admin/slide/add.aspx" class="btn btn-success"><i class="fa fa-camera"></i> thêm ảnh vào slide</a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foot" Runat="Server">
</asp:Content>

