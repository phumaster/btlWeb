﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_user_Default : System.Web.UI.Page
{
    DataControl data = new DataControl();
    protected void Page_Load(object sender, EventArgs e)
    {
        string page = Request.QueryString["page"];
        int currentPage = 1;
        string query = Request.QueryString["q"];
        if (page != null)
        {
            currentPage = Convert.ToInt32(page);
        }
        if (!IsPostBack)
        {
            DataTable u = data.paginateUser(currentPage);
            listUser.DataSource = u;
            DataBind();
            string paginate = data.paginate("users", currentPage);
            pagination.InnerHtml = paginate;
        }
    }

    protected void handle_search(object sender, EventArgs e)
    {
        string query = searchUser.Text;
        DataTable u = data.findUser(query);
        listUser.DataSource = u;
        DataBind();
    }

    protected void deleteItems(object sender, CommandEventArgs e)
    {
        string page = Request.QueryString["page"];
        int currentPage = 1;
        if (page != null)
        {
            currentPage = Convert.ToInt32(page);
        }
        if (e.CommandName == "destroy")
        {
            int id = Convert.ToInt32(e.CommandArgument);
            data.destroyUser(id);
            Session["flash"] = "<div class=\"text-success\">Đã xóa người dùng.</div>";
            DataTable u = data.paginateUser(currentPage);
            listUser.DataSource = u;
            DataBind();
        }
    }
}