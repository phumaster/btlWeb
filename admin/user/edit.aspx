﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="edit.aspx.cs" Inherits="admin_user_edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" Runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-sm-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4>Đổi thông tin người dùng</h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label>Email</label>
                            <asp:TextBox runat="server" ID="email" CssClass="form-control" ReadOnly></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Mật khẩu</label>
                            <asp:TextBox runat="server" ID="passwd" TextMode="Password" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="passwd" ErrorMessage="Vui lòng nhập mật khẩu." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group">
                            <label>Nhập lại mật khẩu</label>
                            <asp:TextBox runat="server" ID="confPass" CssClass="form-control" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="confPass" ErrorMessage="Vui lòng nhập lại mật khẩu." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group">
                            <label>Họ tên</label>
                            <asp:TextBox runat="server" ID="name" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Tuổi</label>
                            <asp:TextBox runat="server" ID="age" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Địa chỉ</label>
                            <asp:TextBox runat="server" ID="addr" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Giới tính</label>
                            <asp:DropDownList runat="server" ID="sex" CssClass="form-control">
                                <asp:ListItem Value="0">Nam</asp:ListItem>
                                <asp:ListItem Value="1">Nữ</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <label>Quyền</label>
                            <asp:DropDownList runat="server" ID="drPermission" CssClass="form-control">
                                <asp:ListItem Value="1">User</asp:ListItem>
                                <asp:ListItem Value="2">Admin</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <asp:Button runat="server" ID="btnRegister" CssClass="btn btn-primary" Text="Hoàn tất" OnClick="save_user_click"  />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foot" Runat="Server">
</asp:Content>

