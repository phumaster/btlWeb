﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="admin_user_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
    Danh sách User
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="control">
                    <a href="register.aspx" class="btn btn-primary"><i class="fa fa-plus"></i>Thêm người dùng</a>
                    <asp:TextBox runat="server" ID="searchUser" placeholder="Nhập email hoặc tên người dùng cần tìm..." CssClass="search"></asp:TextBox>
                    <asp:Button runat="server" ID="search" Text="Tìm kiếm" CssClass="btn btn-default" OnClick="handle_search"/>
                </div>
                <asp:GridView ID="listUser" runat="server" AutoGenerateColumns="False" CssClass="table">
                    <Columns>
                        <asp:BoundField HeaderText="ID" DataField="id" />
                        <asp:BoundField HeaderText="Tên" DataField="name" />
                        <asp:BoundField HeaderText="Địa chỉ" DataField="addr" />
                        <asp:BoundField HeaderText="Email" DataField="email" />
                        <asp:BoundField HeaderText="Giới tính" DataField="sex" />
                        <asp:BoundField HeaderText="Quyền" DataField="permission" />
                        <asp:TemplateField HeaderText="Xóa">
                            <ItemTemplate>
                                <asp:Button ID="Button1" runat="server" Text="Xóa" OnCommand="deleteItems" CommandName="destroy" CommandArgument='<%# Eval("id") %>' CssClass="btn btn-link" OnClientClick="return confirm('Bạn có thực sự muốn xóa?')"/>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:HyperLinkField DataNavigateUrlFields="id" DataNavigateUrlFormatString="/admin/user/edit.aspx?id={0}" HeaderText="Sửa" Text="Sửa"/>
                    </Columns>
                </asp:GridView>
                <div class="text-center">
                    <literal runat="server" ID="pagination"></literal>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foot" runat="Server">
</asp:Content>

