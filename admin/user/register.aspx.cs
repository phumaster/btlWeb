﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_user_register : System.Web.UI.Page
{
    DataControl data = new DataControl();
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void btnRegister_handleClick(object sender, EventArgs e)
    {
        if (data.checkEmailExists(email.Text))
        {
            Session["flash"] = "<div class=\"text-danger\">Email đã tồn tại, vui lòng chọn email khác.</div>";
        }
        else
        {
            User user = new User();
            user.setEmail(Convert.ToString(email.Text));
            user.setPassword(Convert.ToString(passwd.Text));
            user.setName(Convert.ToString(name.Text));
            user.setAge(Convert.ToInt32(age.Text));
            user.setAddr(Convert.ToString(addr.Text));
            user.setSex(Convert.ToInt32(sex.SelectedValue));
            user.setPermission(Convert.ToInt32(drPermission.SelectedValue));
            data.addUser(user);
            Session["flash"] = "<div class=\"text-danger\">Đã thêm người dùng.</div>";
            Response.Redirect("/admin/user");
        }
    }
}