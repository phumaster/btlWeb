﻿<%@ Page ValidateRequest="false" Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="add.aspx.cs" Inherits="admin_category_add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    Thêm danh mục mới
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" Runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Tiêu đề</label> <span style="color: red">*</span>
                    <asp:TextBox runat="server" ID="title" CssClass="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="title" ForeColor="Red" Display="Dynamic" ErrorMessage="Vui lòng nhập tiêu đề"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group">
                    <label>Mô tả</label>
                    <asp:TextBox runat="server" ID="description" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Button runat="server" Text="Thêm danh mục" ID="btnAddCategory" CssClass="btn btn-primary" OnClick="handleAddCategory_click"/>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foot" Runat="Server">
</asp:Content>

