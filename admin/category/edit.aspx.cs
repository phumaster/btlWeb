﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_category_edit : System.Web.UI.Page
{
    DataControl data = new DataControl();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            try
            {
                int id = Convert.ToInt32(Request.QueryString["id"]);
                Category category = data.getCategory(id);
                title.Text = category.title;
                description.Text = category.description;
            }
            catch (Exception ex)
            {
                Session["flash"] = "<div class=\"text-danger\">Có lỗi xảy ra.</div>";
                Response.Redirect("/admin/category/Default.aspx");
            }
        }
    }

    protected void handleSaveCategory_click(object sender, EventArgs e)
    {
        int id = Convert.ToInt32(Request.QueryString["id"]);
        Category category = new Category();
        User user = (User)Session["user"];
        category.title = title.Text;
        category.description = description.Text;
        category.parent = 0;
        category.user_id = user.getID();
        data.updateCategory(category, id);
        Session["flash"] = "<div class=\"text-success\">Sửa danh mục thành công.</div>";
        Response.Redirect("/admin/category/Default.aspx");
    }
}