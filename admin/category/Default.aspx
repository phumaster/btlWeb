﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="admin_category_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" Runat="Server">
    Danh mục
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" Runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="control">
                    <a href="add.aspx" class="btn btn-primary"><i class="fa fa-pencil"></i> Thêm danh mục</a>
                </div>
                <asp:GridView ID="listCategory" runat="server" AutoGenerateColumns="False" CssClass="table">
                    <Columns>
                        <asp:BoundField HeaderText="ID" DataField="id" />
                        <asp:BoundField HeaderText="Tiêu đề" DataField="title" />
                        <asp:BoundField HeaderText="Mô tả" DataField="description" />
                        <asp:BoundField HeaderText="Người đăng" DataField="user_id" />
                        <asp:TemplateField HeaderText="Xóa">
                            <ItemTemplate>
                                <asp:Button ID="Button1" runat="server" Text="Xóa" CommandName="destroy" OnCommand="deleteItems" CommandArgument='<%# Eval("id") %>' CssClass="btn btn-link text-danger" OnClientClick="return confirm('Bạn có thực sự muốn xóa?')"/>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:HyperLinkField DataNavigateUrlFields="id" DataNavigateUrlFormatString="/admin/category/edit.aspx?id={0}" HeaderText="Sửa" Text="Sửa"/>
                    </Columns>
                </asp:GridView>
                <div class="text-center">
                    <literal runat="server" ID="pagination"></literal>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foot" Runat="Server">
</asp:Content>

