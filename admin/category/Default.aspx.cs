﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_category_Default : System.Web.UI.Page
{
    DataControl data = new DataControl();
    protected void Page_Load(object sender, EventArgs e)
    {
        string page = Request.QueryString["page"];
        int currentPage = 1;
        if(page != null)
        {
            currentPage = Convert.ToInt32(page);
        }
        if(!IsPostBack)
        {
            DataTable categories = data.paginateCategories(currentPage);
            listCategory.DataSource = categories;
            DataBind();

            string paginate = data.paginate("categories", currentPage);
            pagination.InnerHtml = paginate;
        }
    }

    protected void deleteItems(object sender, CommandEventArgs e)
    {
        string page = Request.QueryString["page"];
        int currentPage = 1;
        if (page != null)
        {
            currentPage = Convert.ToInt32(page);
        }
        if (e.CommandName == "destroy")
        {
            int id = Convert.ToInt32(e.CommandArgument);
            data.destroyCategory(id);
            Session["flash"] = "<div class=\"text-success\">Đã xóa danh mục.</div>";
            DataTable categories = data.paginateCategories(currentPage);
            listCategory.DataSource = categories;
            DataBind();
        }
    }
}