﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_category_add : System.Web.UI.Page
{
    DataControl data = new DataControl();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void handleAddCategory_click(object sender, EventArgs e)
    {
        Category category = new Category();
        User user = (User)Session["user"];
        category.title = title.Text;
        category.description = description.Text;
        category.user_id = user.getID();
        category.parent = 0;
        data.addCategory(category);
        Session["flash"] = "<div class=\"text-success\">Đã thêm danh mục mới.</div>";
        Response.Redirect("/admin/category/Default.aspx");
    }
}