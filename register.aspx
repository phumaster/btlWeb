﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="register.aspx.cs" Inherits="register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Đăng ký</title>
    <link href="asset/css/bootstrap.min.css" rel="stylesheet" />
    <link href="asset/css/font-awesome.css" rel="stylesheet" />
    <script src="asset/js/jquery-1.9.1.min.js"></script>
    <script src="asset/js/bootstrap.min.js"></script>
    <style type="text/css">
        * {
            border-radius: 0 !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container" style="margin-top: 80px">
            <div class="row">
                <div class="col-lg-6 col-sm-offset-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Đăng ký</h4>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="input-group">
                                    <label class="input-group-addon"><i class="fa fa-user"></i></label>
                                    <asp:TextBox runat="server" ID="name" CssClass="form-control" placeholder="Họ tên" autocomplete="off"></asp:TextBox>
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="name" ForeColor="Red" Display="Dynamic" ErrorMessage="Vui lòng nhập họ tên."></asp:RequiredFieldValidator>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <label class="input-group-addon"><i class="fa fa-envelope"></i></label>
                                    <asp:TextBox runat="server" ID="email" CssClass="form-control" placeholder="Email" autocomplete="off"></asp:TextBox>
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="email" ForeColor="Red" Display="Dynamic" ErrorMessage="Vui lòng nhập email."></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regexValidateEmail" runat="server" ControlToValidate="email" ForeColor="Red" Display="Dynamic" ErrorMessage="Email không hợp lệ" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <label class="input-group-addon"><i class="fa fa-key"></i></label>
                                    <asp:TextBox runat="server" ID="passwd" TextMode="Password" CssClass="form-control" placeholder="Nhập mật khẩu" autocomplete="off"></asp:TextBox>
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="passwd" ForeColor="Red" Display="Dynamic" ErrorMessage="Vui lòng nhập mật khẩu."></asp:RequiredFieldValidator>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <label class="input-group-addon"><i class="fa fa-key"></i></label>
                                    <asp:TextBox runat="server" ID="confPass" CssClass="form-control" placeholder="Nhập lại mật khẩu"></asp:TextBox>
                                </div>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="confPass" ForeColor="Red" Display="Dynamic" ErrorMessage="Vui lòng nhập lại mật khẩu."></asp:RequiredFieldValidator>
                            </div>
                            <div class="form-group">
                                <asp:Button runat="server" ID="btnRegister" CssClass="btn btn-primary" Text="Đăng ký" OnClick="handle_register" />
                                <a href="/" class="btn btn-default"><i class="fa fa-sign-out"></i>về trang chủ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%
            if (Session["flash"] != null)
            {
                Response.Write("<div class=\"modal fade text-center\" id=\"alert-notify\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\"> <div class=\"modal-dialog\" role=\"document\"> <div class=\"modal-content\"> <div class=\"modal-header\"> <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button> <h4 class=\"modal-title\" id=\"myModalLabel\">Thông báo</h4> </div><div class=\"modal-body\"> <p>" + Session["flash"] + "</p></div></div></div></div>");
                Session["flash"] = null;
            }

        %>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#alert-notify').modal('show');
            });
        </script>
    </form>
</body>
</html>
