﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class edit_profile : System.Web.UI.Page
{
    DataControl data = new DataControl();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataBind();
            try
            {
                User you = (User)Session["user"];
                int id = you.getID();
                User user = data.getUser(id);
                email.Text = user.getEmail();
                name.Text = user.getName();
                age.Text = user.getAge() + "";
                addr.Text = user.getAddr();
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('User không tồn tại hoặc đã bị xóa." + ex.Message + "')</script>");
            }
        }
    }

    protected void saveUser(object sender, EventArgs e)
    {
        User you = (User)Session["user"];
        int id = you.getID();
        if (passwd.Text != confPass.Text)
        {
            Session["flash"] = "<div class=\"text-danger\">Mật khẩu nhập lại không trùng khớp.</div>";
        }
        else
        {
            User user = new User();
            user.setAddr(addr.Text);
            user.setPassword(passwd.Text);
            user.setAge(Convert.ToInt32(age.Text));
            user.setEmail(email.Text);
            user.setName(name.Text);
            user.setSex(Convert.ToInt32(sex.SelectedValue));
            user.setPermission(1);
            data.updateUser(user, id);
            Session["flash"] = "<div class=\"text-success\">Đã lưu thay đổi.</div>";
            Response.Redirect("/profile.aspx");
        }
    }
}