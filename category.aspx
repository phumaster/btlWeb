﻿<%@ Page Title="" Language="C#" MasterPageFile="~/guest.master" AutoEventWireup="true" CodeFile="category.aspx.cs" Inherits="category" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="Server">
    <asp:ListView ID="lvData" runat="server">
        <LayoutTemplate>
            <div class="listView" runat="server">
                <div class="listView-item" style="border-bottom: 1px solid rgba(0,0,0,0.05)" runat="server" id="itemPlaceholder">
                    <div class="row" runat="server">
                        <div class="col-sm-2 text-center" runat="server">
                            <img src="" class="thumbnail img-responsive" />
                        </div>
                        <div class="col-sm-10" runat="server">
                            <div style="font-size: 16px"><a href="#"></a></div>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </LayoutTemplate>
        <ItemTemplate>
            <div class="listView-item" style="border-bottom: 1px solid rgba(0,0,0,0.05)" runat="server">
                <div class="row" runat="server">
                    <div class="col-sm-2 text-center" runat="server">
                        <img src="<%#Eval("thumbnail_url") %>" class="thumbnail img-responsive" />
                    </div>
                    <div class="col-sm-10" runat="server">
                        <div style="font-size: 16px">
                            <h4><a href='/post.aspx?id=<%#Eval("id") %>'><%#Eval("title") %></a></h4>
                        </div>
                        <p><%#Eval("created_at")%></p>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:ListView>
    <div class="text-center">
        <literal runat="server" id="pagination"></literal>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foot" runat="Server">
</asp:Content>

