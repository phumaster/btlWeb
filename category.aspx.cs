﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class category : System.Web.UI.Page
{
    DataControl data = new DataControl();
    protected void Page_Load(object sender, EventArgs e)
    {
        string page = Request.QueryString["page"];
        int currentPage = 1;
        if (page != null)
        {
            currentPage = Convert.ToInt32(page);
        }
        if (!IsPostBack)
        {
            try
            {
                int id = Convert.ToInt32(Request.QueryString["id"]);
                DataTable dt = data.paginatePostsInCategory(currentPage, id);
                lvData.DataSource = dt;
                DataBind();
                string paginate = data.htmlPaginatePostsInCategory(currentPage, id);
                pagination.InnerHtml = paginate;
            } catch (Exception ex)
            {
                Session["flash"] = "<div class=\"text-danger\">Danh mục không tồn tại.</div>";
                Response.Redirect("/");
            }
        }
    }
}