﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["isLogged"] = null;
        Session["user"] = null;
        Session["flash"] = "<div class=\"text-success\">Đăng xuất! Phiên làm việc của bạn đã được xóa.</div>";
        Response.Redirect("/");
    }
}