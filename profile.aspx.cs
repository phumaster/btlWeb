﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class profile : System.Web.UI.Page
{
    DataControl data = new DataControl();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataBind();
            try
            {
                User you = (User)Session["user"];
                int id = you.getID();
                User user = data.getUser(id);
                email.Text = user.getEmail();
                name.Text = user.getName();
                age.Text = user.getAge() + "";
                addr.Text = user.getAddr();
                sex.Text = user.getSexString();
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('User không tồn tại hoặc đã bị xóa." + ex.Message + "')</script>");
            }
        }
    }
}