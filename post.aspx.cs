﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class post : System.Web.UI.Page
{
    DataControl data = new DataControl();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            int id = Convert.ToInt32(Request.QueryString["id"]);
            Post post = data.getPost(id);
            lbContent.Text = post.content;
            lbTitle.Text = post.title;
        } catch (Exception ex)
        {
            Session["flash"] = "<div class=\"text-danger\">Bài viết không tồn tại.<div>";
            Response.Redirect("/");
        }
    }

    protected void addNewComment(object sender, EventArgs e)
    {
        try
        {
            int id = Convert.ToInt32(Request.QueryString["id"]);
            User user = (User)Session["user"];
            Comment comment = new Comment();
            comment.content = commentContent.Text;
            comment.created_at = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            comment.username = user.getName();
            comment.post_id = id;
            data.addComment(comment);
            Session["flash"] = "<div class=\"text-success\">Bình luận của bạn đã được đăng.<div>";
            DataBind();
        } catch (Exception ex)
        {
            ;
        }
    }
}