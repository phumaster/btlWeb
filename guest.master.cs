﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class guest : System.Web.UI.MasterPage
{
    DataControl data = new DataControl();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            DataTable categories = data.listCategories();
            menuCategories.DataSource = categories;
            DataBind();

            DataTable lastestPosts = data.posts(5);
            ListView1.DataSource = lastestPosts;
            DataBind();
        }
    }
}
