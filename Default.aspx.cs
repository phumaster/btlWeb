﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    DataControl data = new DataControl();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {

            List<Category> categories = data.getCategoriesAsArray();
            String html = "";

            foreach(Category category in categories)
            {
                html += "<div class=\"fragment\"><div class=\"fragment-header\"><div class=\"block-title\"><a href=\"/category.aspx?id="+category.id+"\">";
                html += category.title;
                html += "</a></div></div><div class=\"fragment-body\"><div class=\"listView\">";
                foreach (Post post in data.getPostsAsArray(category.id, 5))
                {
                    html += "<div class=\"row\"><div class=\"col-sm-2\">";
                    html += "<img src=\""+ post.thumbnail_url + "\" class=\"img-responsive\"/>";
                    html += "</div><div class=\"col-sm-10\">";
                    html += "<a href=\"/post.aspx?id="+post.id+"\">";
                    html += post.title;
                    html += "</a><small style=\"color: #888\">";
                    html += post.created_at;
                    html += "</small></div></div>";
                }
                html += "</div></div></div>";
            }
            listCategories.InnerHtml = html;
        }
    }
}