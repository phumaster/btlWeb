﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Session["isLogged"] == null)
        {
            Response.Redirect("/login.aspx");
        }
        else
        {
            User user = (User) Session["user"];
            if(user.getPermission() != 2)
            {
                Session["flash"] = "<div class=\"text-danger\">Bạn không đủ quyền truy cập.</div>";
                Response.Redirect("/");
            }
        }
    }
}
