﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="profile.aspx.cs" Inherits="profile" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/asset/css/bootstrap.min.css" rel="stylesheet" />
    <script src="/asset/js/jquery-1.9.1.min.js"></script>
    <script src="/asset/js/bootstrap.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>Hồ sơ</h4>
        </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-2">
          <h5>Tên</h5>
        </div>
        <div class="col-sm-8">
            <asp:Label ID="name" runat="server" Text=""></asp:Label>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-2">
          <h5>Email</h5>
        </div>
        <div class="col-sm-8">
            <asp:Label ID="email" runat="server" Text=""></asp:Label>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-2">
          <h5>Địa chỉ</h5>
        </div>
        <div class="col-sm-8">
            <asp:Label ID="addr" runat="server" Text=""></asp:Label>
        </div>
      </div>
        <div class="row">
        <div class="col-sm-2">
          <h5>Tuổi</h5>
        </div>
        <div class="col-sm-8">
            <asp:Label ID="age" runat="server" Text=""></asp:Label>
        </div>
      </div>
        <div class="row">
        <div class="col-sm-2">
          <h5>Giới tính</h5>
        </div>
        <div class="col-sm-8">
            <asp:Label ID="sex" runat="server" Text=""></asp:Label>
        </div>
      </div>
        <div class="row">
            <div class="col-sm-12">
                <a href="/"><i class="glyphicon glyphicon-home"></i> về trang chủ</a> | 
                <a href="/edit_profile.aspx"><i class="glyphicon glyphicon-arrow-right"></i> sửa thông tin</a> 
            </div>
        </div>
    </div>
  </div>
    </div>
    </form>
    <%
        if (Session["flash"] != null)
        {
            Response.Write("<div class=\"modal fade text-center\" id=\"alert-notify\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\"> <div class=\"modal-dialog\" role=\"document\"> <div class=\"modal-content\"> <div class=\"modal-header\"> <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button> <h4 class=\"modal-title\" id=\"myModalLabel\">Thông báo</h4> </div><div class=\"modal-body\"> <p>" + Session["flash"] + "</p></div></div></div></div>");
            Session["flash"] = null;
        }

    %>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#alert-notify').modal('show');
        });
    </script>
</body>
</html>
