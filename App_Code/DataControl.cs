﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class DataControl
{
    SqlConnection sqlConnection;
    public DataControl()
    {
    }

    private void ConnectData()
    {
        string sqlconnect = "Data Source=AMAZING;Initial Catalog=btlWeb;Integrated Security=True";
        sqlConnection = new SqlConnection(sqlconnect);
    }

    public DataTable listUser()
    {
        DataTable tb = new DataTable();
        ConnectData();
        string sql = "select * from users";
        SqlDataAdapter da = new SqlDataAdapter(sql, sqlConnection);
        da.Fill(tb);
        return tb;
    }

    public DataTable paginateUser(int currentPage)
    {
        DataTable tb = new DataTable();
        ConnectData();
        int rowStart = currentPage <= 0 ? 0 : (currentPage - 1) * 10;
        int rowEnd = rowStart + 10;
        string sql = "select * from (select Row_Number() over (order by id) as RowIndex, * from users) as aliasTable Where aliasTable.RowIndex > "+rowStart+" and aliasTable.RowIndex <= "+rowEnd;
        SqlDataAdapter da = new SqlDataAdapter(sql, sqlConnection);
        da.Fill(tb);
        return tb;
    }

    public DataTable paginateCategories(int currentPage)
    {
        DataTable tb = new DataTable();
        ConnectData();
        int rowStart = currentPage <= 0 ? 0 : (currentPage - 1) * 10;
        int rowEnd = rowStart + 10;
        string sql = "select * from (select Row_Number() over (order by id) as RowIndex, * from categories) as aliasTable Where aliasTable.RowIndex > " + rowStart + " and aliasTable.RowIndex <= " + rowEnd;
        SqlDataAdapter da = new SqlDataAdapter(sql, sqlConnection);
        da.Fill(tb);
        return tb;
    }

    public DataTable paginatePosts(int currentPage)
    {
        DataTable tb = new DataTable();
        ConnectData();
        int rowStart = currentPage <= 0 ? 0 : (currentPage - 1) * 10;
        int rowEnd = rowStart + 10;
        string sql = "select * from (select Row_Number() over (order by id) as RowIndex, * from posts) as aliasTable Where aliasTable.RowIndex > " + rowStart + " and aliasTable.RowIndex <= " + rowEnd;
        SqlDataAdapter da = new SqlDataAdapter(sql, sqlConnection);
        da.Fill(tb);
        return tb;
    }

    public DataTable paginatePostsInCategory(int currentPage, int categoryID)
    {
        DataTable tb = new DataTable();
        ConnectData();
        int rowStart = currentPage <= 0 ? 0 : (currentPage - 1) * 10;
        int rowEnd = rowStart + 10;
        string sql = "select * from (select Row_Number() over (order by id) as RowIndex, * from posts) as aliasTable Where category_id = "+categoryID+" and aliasTable.RowIndex > " + rowStart + " and aliasTable.RowIndex <= " + rowEnd;
        SqlDataAdapter da = new SqlDataAdapter(sql, sqlConnection);
        da.Fill(tb);
        return tb;
    }

    public string paginate(string table, int currentPage)
    {
        ConnectData();
        sqlConnection.Open();
        string sql = "select * from "+table;
        string pagination = "<ul class=\"pagination\">";
        SqlCommand cmd = new SqlCommand(sql, sqlConnection);
        SqlDataReader rd = cmd.ExecuteReader();
        int i = 0, totalRecord = 0;
        while (rd.Read())
        {
            totalRecord++;
        }
        int totalPage = (int)Math.Ceiling(totalRecord/10.0);
        while(i < totalPage)
        {
            i++;
            if (i == currentPage)
            {
                pagination += "<li class=\"active\"><a href=\"?page=" + i + "\">" + i + "</a></li>";
            }
            else
            {
                pagination += "<li><a href=\"?page=" + i + "\">" + i + "</a></li>";
            }
        }
        pagination += "</ul>";
        return pagination;
    }

    public string htmlPaginatePostsInCategory(int currentPage, int categoryID)
    {
        ConnectData();
        sqlConnection.Open();
        string sql = "select * from posts where category_id = "+categoryID;
        string pagination = "<ul class=\"pagination\">";
        SqlCommand cmd = new SqlCommand(sql, sqlConnection);
        SqlDataReader rd = cmd.ExecuteReader();
        int i = 0, totalRecord = 0;
        while (rd.Read())
        {
            totalRecord++;
        }
        int totalPage = (int)Math.Ceiling(totalRecord / 10.0);
        while (i < totalPage)
        {
            i++;
            if (i == currentPage)
            {
                pagination += "<li class=\"active\"><a href=\"?id="+categoryID+"&page=" + i + "\">" + i + "</a></li>";
            }
            else
            {
                pagination += "<li><a href=\"?id="+categoryID+"&page=" + i + "\">" + i + "</a></li>";
            }
        }
        pagination += "</ul>";
        return pagination;
    }
    public User getUser(int id)
    {
        User user = new User();
        ConnectData();
        sqlConnection.Open();
        string sql = "select * from users where id=@id";
        SqlCommand cmd = new SqlCommand(sql, sqlConnection);
        cmd.Parameters.AddWithValue("id", id);
        SqlDataReader rd = cmd.ExecuteReader();
        if (rd.Read())
        {
            user.setId(Convert.ToInt32(rd["id"]));
            user.setEmail(Convert.ToString(rd["email"]));
            user.setPassword(Convert.ToString(rd["passwd"]));
            user.setAge(Convert.ToInt32(rd["age"]));
            user.setName(Convert.ToString(rd["name"]));
            user.setAddr(Convert.ToString(rd["addr"]));
            user.setSex(Convert.ToInt32(rd["sex"]));
            user.setPermission(Convert.ToInt32(rd["permission"]));
        }
        return user;
    }
    public void addUser(User user)
    {
        ConnectData();
        string sql = "insert into users values(@email, @passwd, @name, @age, @addr, @sex, @permission)";
        sqlConnection.Open();
        SqlCommand cmd = new SqlCommand(sql, sqlConnection);
        cmd.Parameters.AddWithValue("email", user.getEmail());
        cmd.Parameters.AddWithValue("passwd", user.getPasswd());
        cmd.Parameters.AddWithValue("name", user.getName());
        cmd.Parameters.AddWithValue("age", user.getAge());
        cmd.Parameters.AddWithValue("addr", user.getAddr());
        cmd.Parameters.AddWithValue("sex", user.getSex());
        cmd.Parameters.AddWithValue("permission", user.getPermission());
        cmd.ExecuteNonQuery();
        sqlConnection.Close();

    }

    public void addComment(Comment comment)
    {
        ConnectData();
        string sql = "insert into comments values(@content, @created_at, @username, @post_id)";
        sqlConnection.Open();
        SqlCommand cmd = new SqlCommand(sql, sqlConnection);
        cmd.Parameters.AddWithValue("content", comment.content);
        cmd.Parameters.AddWithValue("created_at", comment.created_at);
        cmd.Parameters.AddWithValue("username", comment.username);
        cmd.Parameters.AddWithValue("post_id", comment.post_id);
        cmd.ExecuteNonQuery();
        sqlConnection.Close();

    }

    public void updateUser(User user, int id)
    {
        ConnectData();
        string sql = "update users set email = @email, passwd = @passwd, name = @name, age = @age, sex = @sex, addr = @addr, permission = @permission where id = @id";
        sqlConnection.Open();
        SqlCommand cmd = new SqlCommand(sql, sqlConnection);
        cmd.Parameters.AddWithValue("id", id);
        cmd.Parameters.AddWithValue("email", user.getEmail());
        cmd.Parameters.AddWithValue("passwd", user.getPasswd());
        cmd.Parameters.AddWithValue("name", user.getName());
        cmd.Parameters.AddWithValue("age", user.getAge());
        cmd.Parameters.AddWithValue("sex", user.getSex());
        cmd.Parameters.AddWithValue("addr", user.getAddr());
        cmd.Parameters.AddWithValue("permission", user.getPermission());
        cmd.ExecuteNonQuery();
        sqlConnection.Close();

    }
    public void destroyUser(int id)
    {
        ConnectData();
        string sql = "delete from users where id=@id";
        sqlConnection.Open();
        SqlCommand cmd = new SqlCommand(sql, sqlConnection);
        cmd.Parameters.AddWithValue("id", id);

        cmd.ExecuteNonQuery();
        sqlConnection.Close();

    }

    public DataTable findUser(string q)
    {
        DataTable tb = new DataTable();
        ConnectData();
        string sql = "select * from users where email like '%"+q+"%' or name like '%"+q+"%'";
        SqlDataAdapter da = new SqlDataAdapter(sql, sqlConnection);
        da.Fill(tb);
        return tb;
    }

    //

    public DataTable listCategories()
    {
        DataTable tb = new DataTable();
        ConnectData();
        string sql = "select * from categories";
        SqlDataAdapter da = new SqlDataAdapter(sql, sqlConnection);
        da.Fill(tb);
        return tb;
    }

    //

    public DataTable listPost()
    {
        DataTable tb = new DataTable();
        ConnectData();
        string sql = "select * from posts";
        SqlDataAdapter da = new SqlDataAdapter(sql, sqlConnection);
        da.Fill(tb);
        return tb;
    }
    public Post getPost(int id)
    {
        Post post = new Post();
        ConnectData();
        sqlConnection.Open();
        string sql = "select * from posts where id=@id";
        SqlCommand cmd = new SqlCommand(sql, sqlConnection);
        cmd.Parameters.AddWithValue("id", id);
        SqlDataReader rd = cmd.ExecuteReader();
        if (rd.Read() != null)
        {
            post.title = Convert.ToString(rd["title"]);
            post.id = Convert.ToInt32(rd["id"]);
            post.content = Convert.ToString(rd["content"]);
            post.category_id = Convert.ToInt32(rd["category_id"]);
            post.thumbnail_url = Convert.ToString(rd["thumbnail_url"]);
            post.user_id = Convert.ToInt32(rd["user_id"]);
            post.created_at = Convert.ToString(rd["created_at"]);
        }
        return post;
    }
    public void addPost(Post post)
    {
        ConnectData();
        string sql = "insert into posts values(@title, @content, @created_at, @see, @category_id, @thumbnail_url, @user_id)";
        sqlConnection.Open();
        SqlCommand cmd = new SqlCommand(sql, sqlConnection);
        cmd.Parameters.AddWithValue("title", post.title);
        cmd.Parameters.AddWithValue("content", post.content);
        cmd.Parameters.AddWithValue("created_at", post.created_at);
        cmd.Parameters.AddWithValue("see", post.see);
        cmd.Parameters.AddWithValue("category_id", post.category_id);
        cmd.Parameters.AddWithValue("thumbnail_url", post.thumbnail_url);
        cmd.Parameters.AddWithValue("user_id", post.user_id);
        cmd.ExecuteNonQuery();
        sqlConnection.Close();

    }

    public void updatePost(Post post, int id)
    {
        ConnectData();
        string sql = "update posts set title = @title, content = @content, created_at = @created_at, category_id = @category_id, thumbnail_url = @thumbnail_url, user_id = @user_id where id = @id";
        sqlConnection.Open();
        SqlCommand cmd = new SqlCommand(sql, sqlConnection);
        cmd.Parameters.AddWithValue("id", id);
        cmd.Parameters.AddWithValue("title", post.title);
        cmd.Parameters.AddWithValue("content", post.content);
        cmd.Parameters.AddWithValue("created_at", post.created_at);
        cmd.Parameters.AddWithValue("category_id", post.category_id);
        cmd.Parameters.AddWithValue("thumbnail_url", post.thumbnail_url);
        cmd.Parameters.AddWithValue("user_id", post.user_id);
        cmd.ExecuteNonQuery();
        sqlConnection.Close();

    }
    public void destroyPost(int id)
    {
        ConnectData();
        string sql = "delete from posts where id=@id";
        sqlConnection.Open();
        SqlCommand cmd = new SqlCommand(sql, sqlConnection);
        cmd.Parameters.AddWithValue("id", id);

        cmd.ExecuteNonQuery();
        sqlConnection.Close();

    }

    public DataTable listPostsInCategory(int id)
    {
        DataTable tb = new DataTable();
        ConnectData();
        string sql = "select * from posts where category_id = @id";
        SqlDataAdapter da = new SqlDataAdapter(sql, sqlConnection);
        da.SelectCommand.Parameters.AddWithValue("@id", id);
        da.Fill(tb);
        return tb;
    }
    //
    public Category getCategory(int id)
    {
        Category category = new Category();
        ConnectData();
        sqlConnection.Open();
        string sql = "select * from categories where id=@id";
        SqlCommand cmd = new SqlCommand(sql, sqlConnection);
        cmd.Parameters.AddWithValue("id", id);
        SqlDataReader rd = cmd.ExecuteReader();
        if (rd.Read() != null)
        {
            category.title = Convert.ToString(rd["title"]);
            category.id = Convert.ToInt32(rd["id"]);
            category.description = Convert.ToString(rd["description"]);
            category.user_id = Convert.ToInt32(rd["user_id"]);
        }
        return category;
    }
    public void addCategory(Category category)
    {
        ConnectData();
        string sql = "insert into categories values(@title, @description, @parent, @user_id)";
        sqlConnection.Open();
        SqlCommand cmd = new SqlCommand(sql, sqlConnection);
        cmd.Parameters.AddWithValue("title", category.title);
        cmd.Parameters.AddWithValue("description", category.description);
        cmd.Parameters.AddWithValue("parent", category.parent);
        cmd.Parameters.AddWithValue("user_id", category.user_id);
        cmd.ExecuteNonQuery();
        sqlConnection.Close();

    }

    public void updateCategory(Category category, int id)
    {
        ConnectData();
        string sql = "update categories set title = @title, description = @description, parent = @parent, user_id = @user_id where id = @id";
        sqlConnection.Open();
        SqlCommand cmd = new SqlCommand(sql, sqlConnection);
        cmd.Parameters.AddWithValue("title", category.title);
        cmd.Parameters.AddWithValue("description", category.description);
        cmd.Parameters.AddWithValue("parent", category.parent);
        cmd.Parameters.AddWithValue("user_id", category.user_id);
        cmd.Parameters.AddWithValue("id", id);
        cmd.ExecuteNonQuery();
        sqlConnection.Close();

    }
    public void destroyCategory(int id)
    {
        ConnectData();
        string sql = "delete from categories where id=@id";
        sqlConnection.Open();
        SqlCommand cmd = new SqlCommand(sql, sqlConnection);
        cmd.Parameters.AddWithValue("id", id);

        cmd.ExecuteNonQuery();
        sqlConnection.Close();

    }

    //
    public int login(String email, String passwd)
    {
        ConnectData();
        string sql = "select * from users where email = @email and passwd = @passwd";
        sqlConnection.Open();
        SqlCommand cmd = new SqlCommand(sql, sqlConnection);
        cmd.Parameters.AddWithValue("@email", email);
        cmd.Parameters.AddWithValue("@passwd", passwd);
        var rd = cmd.ExecuteReader();
        int id = 0;
        if(rd.Read())
        {
            id = Convert.ToInt32(rd["id"]);
        }
        sqlConnection.Close();
        return id;
    }
    //
    public DataTable categories(int limit)
    {
        DataTable tb = new DataTable();
        ConnectData();
        string sql = "select top "+ limit + " * from categories order by id desc";
        SqlDataAdapter da = new SqlDataAdapter(sql, sqlConnection);
        da.Fill(tb);
        return tb;
    }

    public DataTable getPostsInCategory(int categoryID, int limit)
    {
        DataTable tb = new DataTable();
        ConnectData();
        string sql = "select top " + limit + " * from posts where category_id = "+ categoryID +" order by id desc";
        SqlDataAdapter da = new SqlDataAdapter(sql, sqlConnection);
        da.Fill(tb);
        return tb;
    }

    public DataTable posts(int limit)
    {
        DataTable tb = new DataTable();
        ConnectData();
        string sql = "select top " + limit + " * from posts order by id desc";
        SqlDataAdapter da = new SqlDataAdapter(sql, sqlConnection);
        da.Fill(tb);
        return tb;
    }

    public List<Category> getCategoriesAsArray()
    {
        ConnectData();
        sqlConnection.Open();
        string sql = "select * from categories";
        SqlCommand cmd = new SqlCommand(sql, sqlConnection);
        SqlDataReader rd = cmd.ExecuteReader();
        List<Category> categories = new List<Category>();
        while(rd.Read())
        {
            Category category = new Category();
            category.title = Convert.ToString(rd["title"]);
            category.id = Convert.ToInt32(rd["id"]);
            category.description = Convert.ToString(rd["description"]);
            category.user_id = Convert.ToInt32(rd["user_id"]);
            categories.Add(category);
        }
        return categories;
    }

    public List<Post> getPostsAsArray(int categoryID, int limit)
    {
        ConnectData();
        sqlConnection.Open();
        string sql = "select top " + limit + " * from posts where category_id = " + categoryID + " order by id desc";
        SqlCommand cmd = new SqlCommand(sql, sqlConnection);
        SqlDataReader rd = cmd.ExecuteReader();
        List<Post> posts = new List<Post>();
        while(rd.Read())
        {
            Post post = new Post();
            post.title = Convert.ToString(rd["title"]);
            post.content = Convert.ToString(rd["content"]);
            post.created_at = Convert.ToString(rd["created_at"]);
            post.id = Convert.ToInt32(rd["id"]);
            post.category_id = Convert.ToInt32(rd["category_id"]);
            post.thumbnail_url = Convert.ToString(rd["thumbnail_url"]);
            post.user_id = Convert.ToInt32(rd["user_id"]);
            posts.Add(post);
        }
        return posts;
    }

    public bool checkEmailExists(String email)
    {
        ConnectData();
        string sql = "select * from users where email = @email";
        sqlConnection.Open();
        SqlCommand cmd = new SqlCommand(sql, sqlConnection);
        cmd.Parameters.AddWithValue("@email", email);
        var rd = cmd.ExecuteReader();
        return rd.Read();
    }
}