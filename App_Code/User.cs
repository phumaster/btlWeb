﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for User
/// </summary>
public class User
{
    private int id, age, sex, permission;
    private String email, passwd, name, addr;
    public User() { }

    public int getID()
    {
        return this.id;
    }

    public String getEmail()
    {
        return this.email;
    }

    public String getPasswd()
    {
        return this.passwd;
    }

    public String getName()
    {
        return this.name;
    }

    public String getAddr()
    {
        return this.addr;
    }

    public int getAge()
    {
        return this.age;
    }

    public int getSex()
    {
        return this.sex;
    }

    public String getSexString()
    {
        return this.sex == 0 ? "Nam" : "Nữ";
    }

    public int getPermission()
    {
        return this.permission;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public void setSex(int sex)
    {
        this.sex = sex;
    }

    public void setPermission(int permission)
    {
        this.permission = permission;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public void setPassword(String passwd)
    {
        this.passwd = passwd;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setAddr(String addr)
    {
        this.addr = addr;
    }
}