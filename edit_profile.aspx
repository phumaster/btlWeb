﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="edit_profile.aspx.cs" Inherits="edit_profile" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sửa thông tin</title>
    <link href="asset/css/bootstrap.min.css" rel="stylesheet" />
    <link href="asset/css/font-awesome.css" rel="stylesheet" />
    <script src="asset/js/jquery-1.9.1.min.js"></script>
    <script src="asset/js/bootstrap.min.js"></script>
    <style type="text/css">
        * {
            border-radius: 0 !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-sm-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4>Đổi thông tin người dùng</h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label>Email</label>
                            <asp:TextBox runat="server" ID="email" CssClass="form-control" ReadOnly></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Mật khẩu</label>
                            <asp:TextBox runat="server" ID="passwd" TextMode="Password" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="passwd" ErrorMessage="Vui lòng nhập mật khẩu." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group">
                            <label>Nhập lại mật khẩu</label>
                            <asp:TextBox runat="server" ID="confPass" CssClass="form-control" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="confPass" ErrorMessage="Vui lòng nhập lại mật khẩu." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group">
                            <label>Họ tên</label>
                            <asp:TextBox runat="server" ID="name" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Tuổi</label>
                            <asp:TextBox runat="server" ID="age" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Địa chỉ</label>
                            <asp:TextBox runat="server" ID="addr" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Giới tính</label>
                            <asp:DropDownList runat="server" ID="sex" CssClass="form-control">
                                <asp:ListItem Value="0">Nam</asp:ListItem>
                                <asp:ListItem Value="1">Nữ</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <asp:Button runat="server" ID="btnRegister" CssClass="btn btn-primary" Text="Hoàn tất" OnClick="saveUser" />
                             <a href="/profile.aspx" class="btn btn-default">Hủy</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </form>
    <%
        if (Session["flash"] != null)
        {
            Response.Write("<div class=\"modal fade text-center\" id=\"alert-notify\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\"> <div class=\"modal-dialog\" role=\"document\"> <div class=\"modal-content\"> <div class=\"modal-header\"> <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button> <h4 class=\"modal-title\" id=\"myModalLabel\">Thông báo</h4> </div><div class=\"modal-body\"> <p>" + Session["flash"] + "</p></div></div></div></div>");
            Session["flash"] = null;
        }

    %>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#alert-notify').modal('show');
        });
    </script>
</body>
</html>
