﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="admin_login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../asset/css/admin.css" rel="stylesheet" />
    <link href="../asset/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../asset/css/font-awesome.css" rel="stylesheet" />
    <script src="../asset/js/jquery-1.9.1.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="margin-top: 80px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4>Đăng nhập</h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <asp:Label runat="server" ID="lbMessage" Text="" ForeColor="Red"></asp:Label>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="email" ForeColor="Red" Display="Dynamic" ErrorMessage="Vui lòng nhập email."></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regexValidateEmail" runat="server" ControlToValidate="email" ForeColor="Red" Display="Dynamic" ErrorMessage="Email không hợp lệ" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            <div class="input-group">
                                <label class="input-group-addon"><i class="fa fa-envelope-o"></i></label>
                                <asp:TextBox runat="server" placeholder="Nhập email..." ID="email" CssClass="form-control" ></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="passwd" ForeColor="Red" Display="Dynamic" ErrorMessage="Vui lòng nhập mật khẩu."></asp:RequiredFieldValidator>
                            <div class="input-group">
                                <label class="input-group-addon"><i class="fa fa-key"></i></label>
                                <asp:TextBox runat="server" placeholder="Nhập mật khẩu..." ID="passwd" TextMode="Password" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Button Text="Đăng nhập" runat="server" ID="btnLogin" CssClass="btn btn-primary" OnClick="handleBtnLogin_Click"/>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="/" class="btn btn-default"><i class="fa fa-sign-out"></i> về trang chủ</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </form>
</body>
</html>
