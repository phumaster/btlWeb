﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class register : System.Web.UI.Page
{
    DataControl data = new DataControl();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void handle_register(object sender, EventArgs e)
    {
        if(passwd.Text != confPass.Text)
        {
            Session["flash"] = "<div class=\"text-danger\">Mật khẩu nhập lại không trùng khớp.</div>";
        }
        else
        {
            if(data.checkEmailExists(email.Text)) {
                Session["flash"] = "<div class=\"text-danger\">Email đã tồn tại, vui lòng chọn email khác.</div>";
            }
            else
            {
                try
                {
                    User user = new User();
                    user.setAddr("");
                    user.setAge(0);
                    user.setEmail(email.Text);
                    user.setName(name.Text);
                    user.setPassword(passwd.Text);
                    user.setPermission(1);
                    user.setSex(0);
                    data.addUser(user);
                    Session["flash"] = "<div class=\"text-success\">Đăng ký thành công.</div>";
                } catch (Exception ex)
                {
                    Session["flash"] = "<div class=\"text-danger\">Đăng ký thất bại.</div>";
                }
            }
        }
    }
}