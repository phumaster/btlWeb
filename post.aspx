﻿<%@ Page Title="" Language="C#" MasterPageFile="~/guest.master" AutoEventWireup="true" CodeFile="post.aspx.cs" Inherits="post" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="Server">
    <div class="article">
        <h4>
            <asp:Label runat="server" ID="lbTitle" Text=""></asp:Label></h4>
        <div class="article-content">
            <asp:Label runat="server" ID="lbContent" Text=""></asp:Label>
        </div>
        <hr />
        <h4>Bình luận</h4>
        <%
            if (Session["isLogged"] == null)
            {
                Response.Write("<div class=\"alert alert-info\">Vui lòng <a href=\"/login.aspx\">đăng nhập</a> để bình luận.</div>");
            }
            else
            {
        %>
        <asp:TextBox runat="server" ID="commentContent" CssClass="form-control" TextMode="MultiLine" placeholder="Viết bình luận..."></asp:TextBox>
        <br />
        <asp:Button runat="server" ID="btnComment" CssClass="btn btn-primary" Text="Viết bình luận" OnClick="addNewComment" />
        <%
            }
        %>

        <hr />
        <asp:ListView ID="listComment" runat="server" DataSourceID="SqlDataSource1">
            <ItemTemplate>
                <div class="row">
                    <div class="col-sm-12">
                        <b><i class="fa fa-comment-o"></i> <asp:Label ID="NameLabel" runat="server"
                            Text='<%#Eval("username") %>' /></b> 
                         : 
                        <asp:Label ID="Label1" runat="server"
                            Text='<%#Eval("content") %>' />
                    </div>
                </div>
                </tr>
            </ItemTemplate>
        </asp:ListView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:btlWebConnectionString %>" SelectCommand="SELECT * FROM [comments] WHERE ([post_id] = @post_id) ORDER BY [id] DESC">
            <SelectParameters>
                <asp:QueryStringParameter DefaultValue="id" Name="post_id" QueryStringField="id" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foot" runat="Server">
</asp:Content>

