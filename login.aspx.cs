﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_login : System.Web.UI.Page
{
    DataControl data = new DataControl();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void handleBtnLogin_Click(object sender, EventArgs e)
    {
        string mail = email.Text;
        string pass = passwd.Text;

        if(mail != "" && pass != "")
        {
            int userID = data.login(mail, pass);
            if (userID != 0)
            {
                User user = data.getUser(userID);
                Session["isLogged"] = true;
                Session["user"] = user;
                Session["flash"] = "<div class=\"text-success\">Đăng nhập thành công.</div>";
                if(user.getPermission() == 2)
                {
                    Response.Redirect("/admin");
                }
                else
                {
                    Response.Redirect("/");
                }
            }
            else
            {
                lbMessage.Text = "Email hoặc mật khẩu bạn nhập không hợp lệ, vui lòng kiểm tra lại.";
            }
        }
        else
        {
            lbMessage.Text = "Vui lòng nhập đầy đủ thông tin";
        }
    }
}