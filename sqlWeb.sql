create database btlWeb;
go

use btlWeb;
go

create table users(
id int identity primary key,
email nvarchar(50),
passwd nvarchar(255),
name nvarchar(255),
age int,
addr nvarchar(255),
sex int,
permission int default(1),
)
go

create table posts(
id int identity primary key,
title nvarchar(255),
content text,
created_at datetime,
see int,
category_id int,
thumbnail_url text,
user_id int,
)
go

create table categories(
id int identity primary key,
title nvarchar(255),
description nvarchar(255),
parent int default 0,
user_id int,
)
go

create table comments(
id int identity primary key,
content varchar(MAX),
created_at datetime,
username nvarchar(255),
post_id int,
)
go

/* alter table posts add foreign key (user_id) references users(id);
alter table posts add foreign key (category_id) references categories(id);
alter table categories add foreign key (user_id) references users(id);
alter table comments add foreign key (post_id) references posts(id); */